package com.wildSpring.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wildSpring.mvc.component.Employee;
import com.wildSpring.mvc.repository.EmployeeDaoImpl;

@Service
public class EmployeeServImpl {

	@Autowired
	EmployeeDaoImpl empdao;

	public List<Employee> getAllEmployees() {
		return empdao.getAllEmployeesFromDb();
	}
}