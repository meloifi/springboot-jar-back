package com.wildSpring.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wildSpring.mvc.service.EmployeeServImpl;

@RestController
public class TestCtrl {

	@Autowired
	EmployeeServImpl empService;

	// Method to test the angular fetch call.
	@RequestMapping(value= "/test", method= RequestMethod.GET) 
	public String getAllEmployees() {
		return "test number 3";
	}
}